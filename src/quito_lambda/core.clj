(ns quito-lambda.core
  (:require [jackdaw.serdes.edn :as jse]
            [jackdaw.client :as jc]
            [jackdaw.admin :as ja]
            [jackdaw.client.log :as jl])
  (:import (org.apache.kafka.common.serialization Serdes)))

(def app-config
  {"bootstrap.servers" "localhost:29092"})

(def admin-client
  (ja/->AdminClient app-config))

(def producer-serdes
  {:key-serde (jse/serde)
   :value-serde (jse/serde)})

(def consumer-serdes
  {:key-serde (jse/serde)
   :value-serde (jse/serde)})

(def consumer-config
  (assoc app-config "group.id" "prueba-consumer"))

(defonce monitor-topic? (atom true))
(defn monitor-topic []
  (reset! monitor-topic? true)
  (future
    (with-open [my-consumer (-> (jc/consumer
                                 consumer-config consumer-serdes)
                                (jc/subscribe
                                 [{:topic-name "prueba"}]))]
      (loop [results (jc/poll my-consumer 200)]
        (doseq [{:keys [key value timestamp]} results]
          (println "Key: " key "\n Value: " value "\n Timestamp: " timestamp))
        (if @monitor-topic?
          (recur (jc/poll my-consumer 200))
          nil)))))

(defn stop-monitor-topic []
  (reset! monitor-topic? false))

(comment

  ;; Admin Client API

  ;; Indica los topic presentes en el cluster de kafka
  (ja/list-topics admin-client)

  ;; Crear un topic llamado prueba
  (ja/create-topics! admin-client
                     [{:topic-name "prueba"
                       :replication-factor 1
                       :partition-count 1
                       :topic-config {}}])

  ;; Podemos revisar que el topic prueba fue creado correctamente
  (ja/list-topics admin-client)

  ;; Podemos revisar los detalles del topic prueba
  (ja/describe-topics admin-client [{:topic-name "prueba"}])

  ;; Podemos borrar el topic prueba con
  (ja/delete-topics! admin-client [{:topic-name "prueba"}])

  ;; Podemos revisar que el topic prueba ha sido borrado
  (ja/list-topics admin-client)

  ;; Podemos revisar el cluster
  (ja/describe-cluster admin-client)

  ;; Client API

  ;; Podemos producir un evento
  (with-open
    [my-producer (jc/producer app-config producer-serdes)]
    @(jc/produce! my-producer {:topic-name "prueba"}
                  {:key 1}
                  {:event-type :test-event
                   :value 1}))

  ;; Nota: Abre un nuevo hijo de cider-repl para el consumer
  ;; Podemos crear un consumer que hara polling e imprimira los eventos
  (with-open
    [my-consumer
     (-> (jc/consumer
          consumer-config consumer-serdes)
         (jc/subscribe
          [{:topic-name "prueba"}]))]
    (doseq [{:keys [key value timestamp]}
            (jl/log my-consumer 200)]
      (println "Key: " key)
      (println "Value: " value)
      (println "Timestamp: " timestamp)))

  ;; Mejor usamos atoms y futures para monitorear continuamente el topic prueba

  (monitor-topic)
  )
