# quito-lambda

Pequeño programa de Clojure para jugar con Kafka usando Jackdaw, este programa fue creado para el quito lambda "Arquitectura dirigida por eventos con Kafka y Clojure".


## Modo de utilización
Estando en la raíz del repositorio.

1. Levantar broker de Kafka utilizando
```bash
docker-compose up -d
```
Para desmontar el broker utilizar:
```bash
docker-compose down
```

2. Para tener la mejor experiencia posible recomiendo utilizar Emacs + Cider

3. Abrir con Emacs ./src/quito_lambda/core.clj

4. Invocar el cider repl con `M-x cider-jack-in`

5. Para ejecutar una expresion de Clojure dentro de Emacs usar `C-c C-e`

6. Diviertete

## License

No hay garantía
